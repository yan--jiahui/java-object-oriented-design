# 笔记

## 1、static静态修饰符

**static概念：**表示静态的可以修饰变量和方法，静态方法只能调用静态的方法和变量，被所有类对象共享，所有对象都可以修改

修饰的成员，属于类的所以可以直接用类名.成员名使用

**注意！！！**

静态方法只能访问静态的成员，不可以直接访问实列成员。

实列方法可以访问静态的成员，也可以访问实列成员。

静态方法中是不可以出现this关键字的。

## 2、extends继承

**extends概念：**继承是java 三大特性之一，继承是从已有的类中派生出新的类， 新的类能吸收已有类的数据属性和行为，并能扩展新的能力。

简单来说，就是你父亲有的东西，你就不用再自己去创建了；减少不必要的创建，实现拿来主义的最好证明。

子类（派生类），父类(基类 或超类)。

语法:

```java
public class 子类名 extends 父类名{}
```

# 作业

## 题目

![Snipaste_2022-04-21_16-16-33](C:\Users\Administrator\Desktop\Snipaste_2022-04-21_16-16-33.png)

学生信息和行为（名称，年龄，所在班级，查看课表，填写听课反馈）

老师信息和行为（名称，年龄，部门名称，查看课表，发布问题）

定义角色类作为父类包含属性（名称，年龄），行为（查看课表）

定义子类：学生类包含属性（所在班级），行为（填写听课反馈）

定义子类：老师类包含属性（部门名称），行为（发布问题）

## 解答

```java
//定义角色类作为父类包含属性（名称，年龄），行为（查看课表）
public class Role {
	String name;//名称
	int age;//年龄
	//行为（查看课表）
	public void schedule() {
		System.out.println(name+"查看课表");
	}
}
```

```java
//定义子类：学生类包含属性（所在班级），行为（填写听课反馈）
public class Student extends Role{
	String classname;//所在班级
	//行为（填写听课反馈）
	public void lecture() {
		System.out.println(classname+age+"岁的"+name+"听的昏昏欲睡");
	}
}
```

```java
//定义子类：老师类包含属性（部门名称），行为（发布问题）
public class Teacher extends Role{
	String departmentname;//部门名称
	//行为（发布问题）
	public void issue() {
		System.out.println(departmentname+age+"岁"+name+"发布问题");
	}
}
```

```java
//测试类
public class Test {

	public static void main(String[] args) {
		Student a=new Student ();
		a.name="小李";
		a.age=16;
		a.classname="下忍班";
		a.schedule();
		a.lecture();
		Teacher k=new Teacher();
		k.name="凯";
		k.age=37;
		k.departmentname="实训部";
		k.schedule();
		k.issue();
	}

}
```

