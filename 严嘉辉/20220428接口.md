# 笔记

## 1、接口

接口用关键字interface来定义

接口不能实列化

接口中的成员都是public修饰的，写不写都是，因为规范的目的是为了公开化。

语法

```java
public **interface** 接口名 {

// 常量

// 抽象方法

} 
```

# 作业

## 题目

声明一个接口Computerable，其中有对图形求面积的方法getArea（）；定义一个圆形类Circle，内有半径属性，通过构造法为半径赋值；定义一个矩形类rectangle，内有属性长和宽，通过构造方法为长宽赋值，使Circle类及rectangle类都实现Computerable接口，并对两个图形类进行测试。

## 解答

```java
//接口
public interface Computerable {
	void getArea();
}
```

```java
//园
public class Circle implements Computerable{
	private double r;
	

	//无参构造法
	public Circle() {
		super();
	}
	
	//有参构造法
	public Circle(double r) {
		super();
		this.r = r;
	}
	//get set
	public double getR() {
		return r;
	}
	
	public void setR(double r) {
		this.r = r;
	}
	
	@Override
	public void getArea() {
	System.out.println("圆的面积："+Math.PI*r*r);	
	}

}
```

/

```java
//长方形
public class Rectangle implements Computerable{
	private double c;
	private double k;
	

	//无参构造法
	public Rectangle() {
		super();
	}
	//有参构造法
	public Rectangle(double c, double k) {
		super();
		this.c = c;
		this.k = k;
	}
	
	//get set
	public double getC() {
		return c;
	}
	public void setC(double c) {
		this.c = c;
	}
	public double getK() {
		return k;
	}
	public void setK(double k) {
		this.k = k;
	}
	@Override
	public void getArea() {
		// TODO Auto-generated method stub
		
		System.out.println("长方形面积："+getC()*getK());
	}

}
```

```java
//测试类
public class Test {

	public static void main(String[] args) {
		Rectangle r = new Rectangle(2,5);
		Circle c = new Circle(3);
		r.getArea();
		r.getArea();
	}

}
```

